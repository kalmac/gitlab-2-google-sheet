#!/bin/sh

set -eux

docker run --rm -it -u $(id -u):$(id -g) -v $PWD:/home/node -w /home/node clasp clasp "$@"
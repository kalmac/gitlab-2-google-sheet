var gTowns = {};

function onOpen(e) {
  // Add menu
  SpreadsheetApp.getUi()
    .createAddonMenu()
    .addItem("Settings", "settings")
    .addItem("Authorize", "authorize")
    .addItem("Logout", "logout")
    .addToUi();
}

function settings() {
  showSettingsDialog();
}

function authorize() {
  showSidebar();
}

function logout() {
  var oAuth2Service = getOAuth2Service();
  oAuth2Service.reset();
}

/**
 * Get a property from user space
 * @param {String} prop name
 * @return {String} prop value
 */
function getUserProp(prop) {
  var properties = PropertiesService.getUserProperties();
  return properties.getProperty(prop);
}

/**
 * Get a property from document space
 * @param {String} prop name
 * @return {String} prop value
 */
function getDocumentProp(prop) {
  var properties = PropertiesService.getDocumentProperties();
  return properties.getProperty(prop);
}

/**
 * Custom function to execute graqhl query and returns
 * results as table.
 * @param {*} query
 * @customfunction
 */
function GRAPHQL(query) {
  // Check Authentification
  var oAuth2Service = getOAuth2Service();
  if (!oAuth2Service) {
    return "Please fill settings in top menu!";
  }
  if (!oAuth2Service.hasAccess()) {
    return "Please authorize application!";
  }

  try {
    var text = fetch(getDocumentProp("GITLAB_URL") + "/api/graphql", query, {
      Authorization: "Bearer " + oAuth2Service.getAccessToken(),
    });

    return parse(text);
  } catch (err) {
    return err;
  }
}

/**
 * Query GraphQL API using UrlFetchApp.
 *
 * @param {string} url Complete API Url
 * @param {string} query  Graphql Query
 * @param {object} headers Can contain authorization bearer.
 * @param {string} method
 * @param {string} contentType
 */
function fetch(
  url,
  query,
  headers,
  method = "post",
  contentType = "application/json"
) {
  var options = {
    method: method,
    contentType: contentType,
    headers: headers,
    payload: JSON.stringify({ query: query }),
  };

  var response = UrlFetchApp.fetch(url, options);
  if (response.getResponseCode() !== 200) {
    throw "Error : response code (" + response.getResponseCode() + ")";
  }

  return response.getContentText();
}

/**
 * Convert to json then flatten it as a two dimensionals array.
 *
 * @param {string} response
 * @return array
 */
function parse(response) {
  var json = JSON.parse(response);
  var lines = [];
  if ('data' in json) {
    var data = json.data;

    for (r in data) {
      var line = [];
      lines.push(line);
      parseObject(data[r], lines, line);
    }
  }
  return lines;
}

function parseObject(object, lines, line) {
  for (var prop in object) {
    var value = object[prop];

    if (typeof value === "object" && Array.isArray(value)) {
      parseArray(value, lines, line);
    } else if (typeof value === "object" && value !== null) {
      parseObject(value, lines, line);
    } else if (typeof value === "string" || typeof value === "number") {
      line.push(value);
    } else {
      line.push("");
    }
  }

  return line.length;
}

function parseArray(array, lines, line) {
  var before = line.slice(); // copy line before array
  parseObject(array.shift(), lines, line); // insert first element

  if (array.length) {
    array.forEach(function (elt) {
      line = before.slice(); // make a copy of before line
      lines.push(line);
      parseObject(elt, lines, line);
    });
    delete before;
  }
}
/*
module.exports.init = function () {
  var response = `
  {
    "data": {
      "mybus": {
        "issues": {
          "nodes": [
            {
              "webPath": "/mybus/leodagan/-/issues/95",
              "state": "opened",
              "createdAt": "2020-04-30T14:10:27Z",
              "closedAt": null,
              "timeEstimate": 28800,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Clément AUSSENAC"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/leodagan/-/issues/94",
              "state": "opened",
              "createdAt": "2020-04-30T13:46:37Z",
              "closedAt": null,
              "timeEstimate": 72000,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Dimitri GLADIEUX CUNHA"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/gauvain/-/issues/21",
              "state": "opened",
              "createdAt": "2020-04-30T13:42:06Z",
              "closedAt": null,
              "timeEstimate": 28800,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Clément AUSSENAC"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/mimir-k8s/-/issues/14",
              "state": "opened",
              "createdAt": "2020-04-29T21:53:33Z",
              "closedAt": null,
              "timeEstimate": 28800,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Ali BOUAZIZ"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/leodagan/-/issues/93",
              "state": "opened",
              "createdAt": "2020-04-29T14:51:02Z",
              "closedAt": null,
              "timeEstimate": 7200,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Kal ABDI"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/kubernetes/-/issues/32",
              "state": "opened",
              "createdAt": "2020-04-29T08:49:16Z",
              "closedAt": null,
              "timeEstimate": 14400,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Ali BOUAZIZ"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/seli/-/issues/13",
              "state": "opened",
              "createdAt": "2020-04-23T14:57:54Z",
              "closedAt": null,
              "timeEstimate": 57600,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Ali BOUAZIZ"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/kubernetes/-/issues/31",
              "state": "opened",
              "createdAt": "2020-04-22T15:15:46Z",
              "closedAt": null,
              "timeEstimate": 3600,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Kal ABDI"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/pere-blaise-v2/-/issues/4",
              "state": "opened",
              "createdAt": "2020-04-22T07:51:01Z",
              "closedAt": null,
              "timeEstimate": 57600,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": []
              }
            },
            {
              "webPath": "/mybus/leodagan/-/issues/78",
              "state": "opened",
              "createdAt": "2020-04-17T12:32:16Z",
              "closedAt": null,
              "timeEstimate": 86400,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Kal ABDI"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/nessa/-/issues/30",
              "state": "opened",
              "createdAt": "2020-04-16T09:31:15Z",
              "closedAt": null,
              "timeEstimate": 28800,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Nathaniel ALCOCK"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/nessa/-/issues/28",
              "state": "opened",
              "createdAt": "2020-04-16T09:24:27Z",
              "closedAt": null,
              "timeEstimate": 28800,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Nathaniel ALCOCK"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/leodagan/-/issues/71",
              "state": "opened",
              "createdAt": "2020-04-16T08:53:58Z",
              "closedAt": null,
              "timeEstimate": 57600,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Dimitri GLADIEUX CUNHA"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/leodagan/-/issues/52",
              "state": "opened",
              "createdAt": "2020-04-03T13:30:36Z",
              "closedAt": null,
              "timeEstimate": 144000,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Dimitri GLADIEUX CUNHA"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/leodagan/-/issues/34",
              "state": "opened",
              "createdAt": "2020-03-25T14:39:47Z",
              "closedAt": null,
              "timeEstimate": 21600,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Nathaniel ALCOCK"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/leodagan/-/issues/32",
              "state": "opened",
              "createdAt": "2020-03-25T14:34:00Z",
              "closedAt": null,
              "timeEstimate": 14400,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Nathaniel ALCOCK"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/leodagan/-/issues/31",
              "state": "opened",
              "createdAt": "2020-03-25T14:28:26Z",
              "closedAt": null,
              "timeEstimate": 43200,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Nathaniel ALCOCK"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/gauvain/-/issues/17",
              "state": "opened",
              "createdAt": "2020-01-13T17:09:20Z",
              "closedAt": null,
              "timeEstimate": 28800,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Kal ABDI"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/gauvain/-/issues/16",
              "state": "opened",
              "createdAt": "2020-01-13T16:52:29Z",
              "closedAt": null,
              "timeEstimate": 129600,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Ali BOUAZIZ"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/gauvain/-/issues/6",
              "state": "opened",
              "createdAt": "2019-12-05T09:22:53Z",
              "closedAt": null,
              "timeEstimate": 57600,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Clément AUSSENAC"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/gauvain/-/issues/1",
              "state": "opened",
              "createdAt": "2019-10-09T13:04:38Z",
              "closedAt": null,
              "timeEstimate": 93600,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Clément AUSSENAC"
                  }
                ]
              }
            }
          ]
        }
      },
      "packages": {
        "issues": {
          "nodes": [
            {
              "webPath": "/mybus/packages/types-common/-/issues/7",
              "state": "opened",
              "createdAt": "2020-05-02T22:52:51Z",
              "closedAt": null,
              "timeEstimate": 14400,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Nathaniel ALCOCK"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/packages/types-common/-/issues/5",
              "state": "opened",
              "createdAt": "2020-04-16T09:18:30Z",
              "closedAt": null,
              "timeEstimate": 28800,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Kal ABDI"
                  }
                ]
              }
            },
            {
              "webPath": "/mybus/packages/types-common/-/issues/3",
              "state": "opened",
              "createdAt": "2020-04-16T09:02:50Z",
              "closedAt": null,
              "timeEstimate": 14400,
              "totalTimeSpent": 0,
              "dueDate": null,
              "assignees": {
                "nodes": [
                  {
                    "name": "Nathaniel ALCOCK"
                  }
                ]
              }
            }
          ]
        }
      }
    }
  }
`;
  console.log(parse(response));
};
*/
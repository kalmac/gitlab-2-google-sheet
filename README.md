# Gitlab 2 Google Sheet

Fill Google Sheet with Gitlab data from APIs

## Requirements

You need [Docker](https://get.docker.com/) and a Google account.

## Installation

```bash
docker build -t clasp .
```

## Login

```bash
./clasp.sh login --no-localhost
```


/**
 * @see https://github.com/gsuitedevs/apps-script-oauth2
 */
function getOAuth2Service() {
  if (!getDocumentProp("GITLAB_URL")) return false;

  return (
    OAuth2.createService("gitlab")

      // Set the endpoint URLs, which are the same for all Google services.
      .setAuthorizationBaseUrl(
        getDocumentProp("GITLAB_URL") + "/oauth/authorize"
      )
      .setTokenUrl(getDocumentProp("GITLAB_URL") + "/oauth/token")

      // Set the client ID and secret, from the Google Developers Console.
      .setClientId(getDocumentProp("CLIENT_ID"))
      .setClientSecret(getDocumentProp("CLIENT_SECRET"))

      // Set the name of the callback function in the script referenced
      // above that should be invoked to complete the OAuth flow.
      .setCallbackFunction("authCallback")

      // Set the property store where authorized tokens should be persisted.
      .setPropertyStore(PropertiesService.getUserProperties())

      // Requests offline access.
      .setParam("response_type", "code")
      .setParam("scope", "api")
  );
}

function showSidebar() {
  var oAuth2Service = getOAuth2Service();
  if (!oAuth2Service.hasAccess()) {
    var authorizationUrl = oAuth2Service.getAuthorizationUrl();
    var template = HtmlService.createTemplate(
      '<a href="<?= authorizationUrl ?>" target="_blank">Authorize</a>. ' +
        "Reopen the sidebar when the authorization is complete."
    );
    template.authorizationUrl = authorizationUrl;
    var page = template.evaluate();
    SpreadsheetApp.getUi().showSidebar(page);
  }
}

function authCallback(request) {
  var oAuth2Service = getOAuth2Service();
  var isAuthorized = oAuth2Service.handleCallback(request);
  if (isAuthorized) {
    return HtmlService.createHtmlOutput("Success! You can close this tab.");
  } else {
    return HtmlService.createHtmlOutput("Denied. You can close this tab");
  }
}

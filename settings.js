/**
 * Opens a dialog. The dialog structure is described in the Settings.html
 * project file.
 */
function showSettingsDialog() {
  var ui = HtmlService.createTemplateFromFile("settings/dialog")
    .evaluate()
    .setWidth(600)
    .setHeight(600)
    .setSandboxMode(HtmlService.SandboxMode.IFRAME);
  SpreadsheetApp.getUi().showModalDialog(ui, "Settings");
  return true;
}

/**
 * Update configuration from user form.
 * @param {Object} data
 */
function updateSettings(data) {
  var properties = PropertiesService.getDocumentProperties();
  properties.setProperty("CLIENT_ID", data.CLIENT_ID);
  properties.setProperty("CLIENT_SECRET", data.CLIENT_SECRET);
  properties.setProperty("GITLAB_URL", data.GITLAB_URL);

  return true;
}
